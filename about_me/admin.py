from django.contrib import admin
from .models import HomeSectionMainText, HomeSectionBackgroundImage, AboutSectionHeader, AboutSectionDescription1, AboutSectionDescription2, AboutSectionProfilePicture, EducationContainer, ExperienceCard, SkillCard, Message

# Register your models here.
admin.site.register(HomeSectionMainText)
admin.site.register(HomeSectionBackgroundImage)
admin.site.register(AboutSectionHeader)
admin.site.register(AboutSectionDescription1)
admin.site.register(AboutSectionDescription2)
admin.site.register(AboutSectionProfilePicture)
admin.site.register(EducationContainer)
admin.site.register(ExperienceCard)
admin.site.register(SkillCard)
admin.site.register(Message)
