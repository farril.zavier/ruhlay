from django import forms

class ContactForm(forms.Form):
    name = forms.CharField(label="Your Name", required=True, max_length=100, widget=forms.TextInput({'class': 'form-control', 'placeholder': 'Enter your name'}))
    message = forms.CharField(required=True, widget=forms.Textarea({'class': 'form-control', 'rows': '3'}))
