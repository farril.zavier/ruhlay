import datetime
from django.db import models
from django.core.exceptions import ValidationError

YEAR_CHOICES = []
for x in range(1900, (datetime.datetime.now().year+1)):
    YEAR_CHOICES.append((str(x), str(x)))
YEAR_CHOICES.append(("Present", "Present"))

def validate_only_one_instance(object):
    model = object.__class__
    if model.objects.count() > 0 and object.pk != model.objects.get().pk:
        raise ValidationError("Can only create 1 %s instance" % model.__name__)

# Create your models here.
class HomeSectionMainText(models.Model):
    def clean(self):
        validate_only_one_instance(self)
    desc = models.CharField(max_length=10)
    text_color = models.CharField(blank=True, max_length=10)

class HomeSectionBackgroundImage(models.Model):
    def clean(self):
        validate_only_one_instance(self)
    img = models.ImageField(blank=True, default='unavailable.png')
    def getImage(self):
        if not self.img:
            return "/static/img/cityscape.jpg"
        else:
            return self.img.url

class AboutSectionHeader(models.Model):
    def clean(self):
        validate_only_one_instance(self)
    header = models.CharField(max_length=100)

class AboutSectionDescription1(models.Model):
    def clean(self):
        validate_only_one_instance(self)
    desc = models.CharField(max_length=300)

class AboutSectionDescription2(models.Model):
    def clean(self):
        validate_only_one_instance(self)
    desc = models.CharField(max_length=300)

class AboutSectionProfilePicture(models.Model):
    def clean(self):
        validate_only_one_instance(self)
    img = models.ImageField(blank=True, default='unavailable.png')
    def getImage(self):
        if not self.img:
            return "/static/img/profilepic.jpg"
        else:
            return self.img.url

class EducationContainer(models.Model):
    start_year = models.CharField(max_length=10, choices=YEAR_CHOICES)
    end_year = models.CharField(max_length=10, choices=YEAR_CHOICES)
    institution = models.CharField(max_length=75)
    extra_info = models.CharField(max_length=50, blank=True)

class ExperienceCard(models.Model):
    thumbnail = models.ImageField(blank=True, default='unavailable.png')
    organization = models.CharField(max_length=50)
    position = models.CharField(max_length=50)
    def getImage(self):
        if not self.thumbnail:
            return "/static/img/unavailable.png"
        else:
            return self.thumbnail.url

class SkillCard(models.Model):
    thumbnail = models.ImageField(blank=True, default='unavailable.png')
    skill = models.CharField(max_length=20)
    def getImage(self):
        if not self.thumbnail:
            return "/static/img/unavailable.png"
        else:
            return self.thumbnail.url

class Message(models.Model):
    name = models.CharField(max_length=100)
    message = models.TextField()
