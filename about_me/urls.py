from django.urls import path
from .views import about_me, contact_me

urlpatterns = [
    path('', about_me, name='about_me'),
    path('contact-me', contact_me, name='contact_me')
]
