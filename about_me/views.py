from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import HomeSectionMainText, HomeSectionBackgroundImage, AboutSectionHeader, AboutSectionDescription1, AboutSectionDescription2, AboutSectionProfilePicture, EducationContainer, ExperienceCard, SkillCard, Message
from .forms import ContactForm

response = {}

# Create your views here.
def about_me(request):
    home_section_main_text = None
    home_section_background_image = None
    about_section_header = None
    about_section_description1 = None
    about_section_description2 = None
    about_section_profile_picture = None
    education_containers = None
    experience_cards = None
    skill_cards = None
    if HomeSectionMainText.objects.all().count() == 1:
        home_section_main_text = HomeSectionMainText.objects.first()
    if HomeSectionBackgroundImage.objects.all().count() == 1:
        home_section_background_image = HomeSectionBackgroundImage.objects.first()
    if AboutSectionHeader.objects.all().count() == 1:
        about_section_header = AboutSectionHeader.objects.first()
    if AboutSectionDescription1.objects.all().count() == 1:
        about_section_description1 = AboutSectionDescription1.objects.first()
    if AboutSectionDescription2.objects.all().count() == 1:
        about_section_description2 = AboutSectionDescription2.objects.first()
    if AboutSectionProfilePicture.objects.all().count() == 1:
        about_section_profile_picture = AboutSectionProfilePicture.objects.first()
    if EducationContainer.objects.all().count() > 0:
        education_containers = EducationContainer.objects.all()
    if ExperienceCard.objects.all().count() > 0:
        experience_cards = ExperienceCard.objects.all()
    if SkillCard.objects.all().count() > 0:
        skill_cards = SkillCard.objects.all()

    response['home_section_main_text'] = home_section_main_text
    response['home_section_background_image'] = home_section_background_image
    response['about_section_header'] = about_section_header
    response['about_section_description1'] = about_section_description1
    response['about_section_description2'] = about_section_description2
    response['about_section_profile_picture'] = about_section_profile_picture
    response['education_containers'] = education_containers
    response['experience_cards'] = experience_cards
    response['skill_cards'] = skill_cards

    response['messages'] = Message.objects.all()
    response['form'] = ContactForm

    return render(request, 'about_me.html', response)

def contact_me(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            Message.objects.create(**data)
            return HttpResponseRedirect('/')
        else:
            return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')
